using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CameraControl : MonoBehaviour
{
    public float scrollSens = 0.05f;
    public float rotateSens = 0.05f;
    public float zoomSens = 0.05f;

    public Vector2 distanceLimits = new Vector2(3, 10);
    public Vector2 angleLimits = new Vector2(30, 70);
    private float _distanceDelta;
    private float _angleDelta;
    
    
    private Transform _transform;
    public Transform _zoomTransform;
    public Transform _cameraTransform;

    private float _zoomDelta = 0.5f;
    private float _prevMousePositionX;
    private float _prevMousePositionY;
    
    private void Awake()
    {
        rotateSens /= 10;
        _transform = transform;
        _distanceDelta = distanceLimits.y - distanceLimits.x;
        _angleDelta = angleLimits.y - angleLimits.x;
    }

    private void Update()
    {
        RotateCameraUpdate();
        ZoomCameraUpdate();
        ScrollCameraUpdate();
    }
    
    private void ZoomCameraUpdate()
    {
        _zoomDelta = Mathf.Lerp(
            _zoomDelta,
            _zoomDelta - Input.mouseScrollDelta.y * zoomSens,
            Time.deltaTime);
        _zoomDelta = Mathf.Clamp01(_zoomDelta);

        _cameraTransform.localPosition = Vector3.back * (distanceLimits.x + _distanceDelta * _zoomDelta);
        _zoomTransform.localRotation = Quaternion.Euler(angleLimits.x + _angleDelta * _zoomDelta, 0, 0);
    }
    
    private void RotateCameraUpdate()
    {
        if (Input.GetMouseButtonDown(1))
        {
            _prevMousePositionX = Input.mousePosition.x;
            _prevMousePositionY = Input.mousePosition.y;
        }

        if (!Input.GetMouseButton(1))
            return;

        var localRotation = _transform.localRotation;
        localRotation = Quaternion.Lerp(
            localRotation,
            localRotation * Quaternion.Euler(
                0, 
                (Input.mousePosition.x - _prevMousePositionX) * rotateSens,
                0),
            Time.fixedTime);
        _transform.localRotation = localRotation;

        _zoomDelta = Mathf.Lerp(
            _zoomDelta,
            _zoomDelta - (Input.mousePosition.y - _prevMousePositionY) * rotateSens * 5,
            Time.deltaTime);
        _zoomDelta = Mathf.Clamp01(_zoomDelta);
        
        
        
        _prevMousePositionX = Input.mousePosition.x;
        _prevMousePositionY = Input.mousePosition.y;
    }
    
    private void ScrollCameraUpdate()
    {
        
    }
}
