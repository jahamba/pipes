using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CellPipe : MonoBehaviour
{
    [SerializeField] private CellPipePrefabsSet cellPipePrefabsSet;

    private GameObject _visualGeometry;
    
    
    public void SetVisualGeometry(string faces)
    {
        if (faces.Count() != 6)
            return;
        if (_visualGeometry == null)
            Destroy(_visualGeometry);
        var connections = faces.Replace("0", "").Length;
        var rot = Quaternion.Euler(0, 0, 0);
        switch (connections)
        {
            case 1:
                _visualGeometry = Instantiate(cellPipePrefabsSet.source, transform);
                rot = faces switch
                {
                    "010000" => Quaternion.Euler(0, 90, 0),
                    "000100" => Quaternion.Euler(0, -90, 0),
                    "000010" => Quaternion.Euler(0, 180, 0),
                    _ => Quaternion.Euler(0, 0, 0)
                };

                
                break;
            case 2:
                if (faces == "100010" || faces == "010100" || faces == "001001")
                {
                    _visualGeometry = Instantiate(cellPipePrefabsSet.horizontal, transform);
                    rot = faces switch
                    {
                        "010100" => Quaternion.Euler(0, 90, 0),
                        "001001" => Quaternion.Euler(90, 90, 0),
                        _ => Quaternion.Euler(0, 0, 0)
                    };
                }
                else
                {
                    _visualGeometry = Instantiate(cellPipePrefabsSet.corner, transform);
                    rot = faces switch
                    {
                        "110000" => Quaternion.Euler(0, 90, 0),
                        "000110" => Quaternion.Euler(0, -90, 0),
                        "010010" => Quaternion.Euler(0, 180, 0),
                        "100001" => Quaternion.Euler(0, 0, 90),
                        "010001" => Quaternion.Euler(0, 90, 90),
                        "000101" => Quaternion.Euler(0, -90, 90),
                        "000011" => Quaternion.Euler(0, 180, 90),
                        "101000" => Quaternion.Euler(0, 0, -90),
                        "011000" => Quaternion.Euler(0, 90, -90),
                        "001100" => Quaternion.Euler(0, -90, -90),
                        "001010" => Quaternion.Euler(0, 180, -90),
                        _ => Quaternion.Euler(0, 0, 0)
                    };
                }
                break;
            case 3:
                _visualGeometry = Instantiate(cellPipePrefabsSet.cross, transform);
                rot = faces switch
                {
                    "110100" => Quaternion.Euler(0, 90, 0),
                    "010110" => Quaternion.Euler(0, -90, 0),
                    "110010" => Quaternion.Euler(0, 180, 0),
                    _ => Quaternion.Euler(0, 0, 0)
                };
                break;
        }
        _visualGeometry.transform.localRotation = rot;
    }
}
