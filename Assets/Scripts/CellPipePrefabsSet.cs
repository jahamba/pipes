using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Pipes/CellPipePrefabsSet")]
public class CellPipePrefabsSet : ScriptableObject
{
    public GameObject source;
    public GameObject horizontal;
    public GameObject corner;
    public GameObject cross;

    
}
