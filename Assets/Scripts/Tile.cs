using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

public class Tile : MonoBehaviour
{

    [SerializeField] private Transform scalerTransform;
    [SerializeField] private CellPipe cellPipePrefab;
    
    private enum LinkType {A,B,C,D}
    
    private static readonly Random Rnd = new Random();
    private static IEnumerable<LinkType> GetRandomLinkTypeCombination()
    {
        var lst = new List<LinkType>();
        
        for (var i = 0; i < 3; i++)
        {
            if (i < 1) lst.Add(LinkType.A);
            if (i < 2)
            {
                lst.Add(LinkType.B);
                lst.Add(LinkType.C);
            }
            lst.Add(LinkType.D);
        }
        
        return lst.OrderBy(x => Rnd.Next());
    }

    private void Awake()
    {
        var lst = GetRandomLinkTypeCombination();
        //Debug.Log(lst.Aggregate("", (current, tp) => current + tp.ToString()));
        var linkTypes = lst as LinkType[] ?? lst.ToArray();

        var cellsDict = new Dictionary<LinkType, bool[,]>()
        {
            {LinkType.B, new bool[4, 4]},
            {LinkType.C, new bool[4, 4]},
            {LinkType.D, new bool[4, 4]}
        };
        
        for (var i = 0; i < linkTypes.Count(); i++)
        {
            var x = i switch
            {
                0 => 1,
                1 => 2,
                2 => 3,
                3 => 3,
                4 => 2,
                5 => 1,
                6 => 0,
                _ => 0
            };
            var z = i switch
            {
                0 => 0,
                1 => 0,
                2 => 1,
                3 => 2,
                4 => 3,
                5 => 3,
                6 => 2,
                _ => 1
            };
            var xc = x - 1.5f;
            var zc = z - 1.5f;

            var cellPipe = Instantiate(cellPipePrefab, scalerTransform);
            cellPipe.transform.localPosition = new Vector3(xc, 0, zc);
            
            if(linkTypes[i]==LinkType.A)
            {
                cellPipe.SetVisualGeometry(
                    i switch
                    {
                        0 => "110000",
                        1 => "100100",
                        2 => "100100",
                        3 => "000110",
                        4 => "000110",
                        5 => "010010",
                        6 => "010010",
                        _ => "110000"
                    });
                
            }
            else
            {
                cellPipe.SetVisualGeometry(
                    i switch
                    {
                        0 => "101000",
                        1 => "101000",
                        2 => "001100",
                        3 => "001100",
                        4 => "001010",
                        5 => "001010",
                        _ => "011000"
                    });
                var yDepth = linkTypes[i] switch
                {
                    LinkType.B => 1,
                    LinkType.C => 2,
                    _ => 3
                };
                for (var yc = 1; yc < yDepth; yc++)
                {
                    cellPipe = Instantiate(cellPipePrefab, scalerTransform);
                    cellPipe.transform.localPosition = new Vector3(xc, -yc, zc);
                    cellPipe.SetVisualGeometry("001001");
                }

                cellPipe = Instantiate(cellPipePrefab, scalerTransform);
                cellPipe.transform.localPosition = new Vector3(xc, - yDepth, zc);
                cellPipe.SetVisualGeometry(
                    i switch
                    {
                        0 => "000011",
                        1 => "000011",
                        2 => "010001",
                        3 => "010001",
                        4 => "100001",
                        5 => "100001",
                        _ => "000101"
                    });

                cellsDict[linkTypes[i]][x, z] = true;
                var cx = i switch
                {
                    2 => x - 1,
                    3 => x - 1,
                    6 => x + 1,
                    7 => x + 1,
                    _ => x
                };
                var cz = i switch
                {
                    0 => z + 1,
                    1 => z + 1,
                    5 => z - 1,
                    4 => z - 1,
                    _ => z
                };
                cellsDict[linkTypes[i]][cx, cz] = true;
            }
        }

        foreach (var cell in cellsDict)
        {

            var arr = new bool[4];
            arr[0] = cell.Value[1, 1];
            arr[1] = cell.Value[2, 1];
            arr[2] = cell.Value[2, 2];
            arr[3] = cell.Value[1, 2];
            var s = arr.Aggregate("", (current, b) => current + (b ? "1" : "0"));
            switch (s)
            {
                case "1010":
                {
                    var xn = cell.Value[0, 1] ? 2 : 1;
                    var zn = cell.Value[0, 1] ? 1 : 2;
                    cellsDict[cell.Key][xn, zn] = true;
                    break;
                }
                case "0101":
                {
                    var xn = cell.Value[3, 1] ? 1 : 2;
                    var zn = cell.Value[3, 1] ? 1 : 2;
                    cellsDict[cell.Key][xn, zn] = true;
                    break;
                }
            }

            for (var x = 1; x < 3; x++)
            {
                for (var z = 1; z < 3; z++)
                {
                    if(!cellsDict[cell.Key][x,z]) continue;

                    arr[0] = cellsDict[cell.Key][x, z - 1];
                    arr[1] = cellsDict[cell.Key][x + 1, z];
                    arr[2] = cellsDict[cell.Key][x, z + 1];
                    arr[3] = cellsDict[cell.Key][x - 1, z];
                    
                    s = arr.Aggregate("", (current, b) => current + (b ? "1" : "0"));
                    var faces = s switch
                    {
                        "1100" => "100100",
                        "0110" => "000110",
                        "0011" => "010010",
                        "1001" => "110000",
                        "1010" => "100010",
                        "0101" => "010100",
                        "1110" => "100110",
                        "1101" => "110100",
                        "1011" => "110010",
                        _ => "010110"
                    };
                    
                    var xc = x - 1.5f;
                    var zc = z - 1.5f;
                    var yc = cell.Key switch
                    {
                        LinkType.B => 1,
                        LinkType.C => 2,
                        _ => 3
                    };
                    
                    var cellPipe = Instantiate(cellPipePrefab, scalerTransform);
                    cellPipe.transform.localPosition = new Vector3(xc, - yc, zc);
                    cellPipe.SetVisualGeometry(faces);
                }
            }
            
        }
    }
}
